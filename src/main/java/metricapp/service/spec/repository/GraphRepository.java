package metricapp.service.spec.repository;

import java.util.ArrayList;

import metricapp.entity.graph.Graph;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface GraphRepository extends MongoRepository<Graph, String> {
	
	public ArrayList<Graph> findAll();

	public Graph findById(String id);

	@Query("{measurementGoalId.instance:?0}")
	public ArrayList<Graph> findByMeasurementGoalId(String measurementGoalId);

}
