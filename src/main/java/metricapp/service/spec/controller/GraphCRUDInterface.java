package metricapp.service.spec.controller;

import java.io.IOException;

import metricapp.dto.graph.GraphCrudDTO;
import metricapp.dto.graph.GraphDTO;
import metricapp.entity.external.NotificationPointerBus;
import metricapp.entity.graph.Graph;
import metricapp.exception.BadInputException;
import metricapp.exception.BusException;
import metricapp.exception.DBException;
import metricapp.exception.IllegalStateTransitionException;
import metricapp.exception.NotFoundException;

public interface GraphCRUDInterface {
	
	GraphDTO createGraphFromNotification(NotificationPointerBus organizationalGoalPointer, String measurementGoalId);
	GraphCrudDTO getAll() throws NotFoundException;
	Graph updateGraph(Graph graph);
	GraphCrudDTO updateGraph(GraphDTO dto) throws DBException, NotFoundException, BadInputException, IllegalStateTransitionException;
	GraphCrudDTO getGraphByMeasurementGoalId(String measurementGoalId) throws NotFoundException;
	GraphCrudDTO getAllApproved() throws BadInputException, BusException, IOException;
	void deleteGraphById(String id) throws BadInputException;

}
