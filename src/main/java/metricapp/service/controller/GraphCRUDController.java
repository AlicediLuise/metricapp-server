package metricapp.service.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

import lombok.Data;
import metricapp.dto.graph.GraphCrudDTO;
import metricapp.dto.graph.GraphDTO;
import metricapp.entity.Entity;
import metricapp.entity.State;
import metricapp.entity.external.NotificationPointerBus;
import metricapp.entity.external.PointerBus;
import metricapp.entity.graph.Graph;
import metricapp.exception.BadInputException;
import metricapp.exception.BusException;
import metricapp.exception.DBException;
import metricapp.exception.IllegalStateTransitionException;
import metricapp.exception.NotFoundException;
import metricapp.service.spec.ModelMapperFactoryInterface;
import metricapp.service.spec.controller.GraphCRUDInterface;
import metricapp.service.spec.repository.BusApprovedElementInterface;
import metricapp.service.spec.repository.GraphRepository;
import metricapp.utility.stateTransitionUtils.AbstractStateTransitionFactory;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service("GraphCRUDController")
public class GraphCRUDController implements GraphCRUDInterface {
	
	@Autowired
	private ModelMapperFactoryInterface modelMapperFactory;
	
	@Autowired
	private BusApprovedElementInterface busApprovedElementRepository;
	
	@Autowired
	private GraphRepository graphRepository;
	
	
	@Override
	public GraphDTO createGraphFromNotification(NotificationPointerBus organizationalGoalPointer, String measurementGoalId) {
	Graph graph = new Graph();
	graph.setState(State.Created);
	graph.setEntityType(Entity.Graph);
	graph.setCreationDate(LocalDate.now());
	graph.setLastVersionDate(LocalDate.now());
	graph.setReleaseNote("Graph generated from Organizational goal " + organizationalGoalPointer.getInstance() + " creation");
	
	PointerBus pointerBusMeasurementGoal = new PointerBus();
	pointerBusMeasurementGoal.setInstance(measurementGoalId);
	pointerBusMeasurementGoal.setTypeObj(Entity.MeasurementGoal.name());
	graph.setMeasurementGoalId(pointerBusMeasurementGoal);
	
	return modelMapperFactory.getStandardModelMapper().map(this.createGraph(graph), GraphDTO.class);
	}


	private Object createGraph(Graph graph) {
		return graphRepository.save(graph);
	}
	
	private GraphDTO graphToDTO(Graph graph){
		ModelMapper modelMapper = modelMapperFactory.getStandardModelMapper();
		GraphDTO dto = modelMapper.map(graph, GraphDTO.class);
		return dto;
	}


	@Override
	public GraphCrudDTO getAll() throws NotFoundException {
		ArrayList<Graph> graphs = graphRepository.findAll();
		if (graphs.size() == 0) {
			throw new NotFoundException("no Graphs");
		}
		GraphCrudDTO dto = new GraphCrudDTO();
		dto.setRequest("All Graphs available on DB" );
		Iterator<Graph> graph = graphs.iterator();
		while (graph.hasNext()) {
			dto.addGraphToList(modelMapperFactory.getLooseModelMapper().map(graph.next(), GraphDTO.class));
		}
		return dto;
	}
	
	@Override
	public GraphCrudDTO getAllApproved() throws BadInputException, BusException, IOException{
		GraphCrudDTO dto = new GraphCrudDTO();
		ArrayList<Graph> graphs = busApprovedElementRepository.getAllApprovedGraph();
		dto.setRequest("All Approved Graphs");
		Iterator<Graph> graphP = graphs.iterator();
		while (graphP.hasNext()) {
			dto.addGraphToList(modelMapperFactory.getStandardModelMapper().map(graphP.next(), GraphDTO.class));
		}
		return dto;
	}
	
	@Override
	public GraphCrudDTO getGraphByMeasurementGoalId(String measurementGoalId) throws NotFoundException {
		ArrayList<Graph> graphs = graphRepository.findByMeasurementGoalId(measurementGoalId);
		if (graphs.size() == 0) {
			throw new NotFoundException("no Graphs");
		}
		GraphCrudDTO dto = new GraphCrudDTO();
		dto.setRequest("Graph, measurementGoalId=" +  measurementGoalId);
		Iterator<Graph> graph = graphs.iterator();
		while (graph.hasNext()) {
			dto.addGraphToList(modelMapperFactory.getLooseModelMapper().map(graph.next(), GraphDTO.class));
		}
		return dto;
	}
	
	
	@Override
	public GraphCrudDTO updateGraph(GraphDTO dto) throws DBException, NotFoundException, BadInputException, IllegalStateTransitionException{
		
		ModelMapper modelMapper = modelMapperFactory.getStandardModelMapper();
		Graph newGraph = modelMapper.map(dto, Graph.class);
		Graph oldGraph = graphRepository.findById(newGraph.getId());
		stateTransition(oldGraph, newGraph);

		GraphCrudDTO dtoCrud = new GraphCrudDTO();
		dtoCrud.setRequest("update Graph id" + dto.getMetadata().getId());
		
		try {
			dtoCrud.addGraphToList(graphToDTO(updateGraph(newGraph)));
		} catch (Exception e) {
			throw new DBException("Error in saving, tipically your version is not the last");
		}

		return dtoCrud;
	}
	
	@Override
	public void deleteGraphById(String id) throws BadInputException {
		if (id == null) {
			throw new BadInputException("Bad Input");
		}
		Graph graph = graphRepository.findById(id);
		graphRepository.delete(graph);
	}
	
	
	private void stateTransition(Graph oldGraph, Graph newGraph) throws IllegalStateTransitionException, NotFoundException {
		
		newGraph.setLastVersionDate(LocalDate.now());
		System.out.println(oldGraph.getState().toString()+"------>"+newGraph.getState().toString());
		if (oldGraph.getState().equals(newGraph.getState())) {
			 return;
			 }
		try {
			AbstractStateTransitionFactory.getFactory(Entity.Graph).transition(oldGraph, newGraph).execute();
		} catch (Exception e) {
			throw new IllegalStateTransitionException(e);
		}
	
	}
	
	@Override
	public Graph updateGraph(Graph graph){
		return graphRepository.save(graph);		
	}
	
	
}
