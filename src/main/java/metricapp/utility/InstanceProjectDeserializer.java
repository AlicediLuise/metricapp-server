package metricapp.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import metricapp.entity.external.InstanceProject;
import metricapp.entity.external.OrganizationalGoal;

public class InstanceProjectDeserializer extends JsonDeserializer<InstanceProject> {
    @Override
    public InstanceProject deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	   ObjectCodec oc = jp.getCodec();
    	   JsonNode node = oc.readTree(jp);
    	   

    	   final String title = node.get("title").asText();
    	   final String description = node.get("description").asText();
    	   final String status = node.get("status").asText();
    	   final String projectManagerId = node.get("projectManager").get("id").asText();

    	   /*
    	    * private String iteration;
    	    * private String rootElementId;
    	    * */
       	   	   
    	   InstanceProject project = new InstanceProject();
    	   project.setTitle(title);
    	   project.setDescription(description);
    	   project.setStatus(status);
    	   project.setProjectManagerId(projectManagerId);
    	   
    	   return project;
    }
}