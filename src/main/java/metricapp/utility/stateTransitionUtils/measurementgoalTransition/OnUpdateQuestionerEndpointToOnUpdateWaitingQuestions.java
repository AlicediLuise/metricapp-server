package metricapp.utility.stateTransitionUtils.measurementgoalTransition;

import metricapp.entity.Element;
import metricapp.entity.event.ArtifactScope;
import metricapp.entity.event.Event;
import metricapp.entity.event.EventPhase;
import metricapp.utility.NotificationService;

public class OnUpdateQuestionerEndpointToOnUpdateWaitingQuestions extends
		MeasurementGoalStateTransitionCommand {

	public OnUpdateQuestionerEndpointToOnUpdateWaitingQuestions(Element before, Element after) {
		super(before, after);
	}
	
	@Override
	public void execute() {
		System.out.println("From questionEndpoint to waitingQuestion");
		Event event = new Event(EventPhase.PHASE2_2, after.getMetricatorId(), ArtifactScope.MGOAL, after.getId(), "WAITING QUESTIONS");
		NotificationService.getInstance().publish("QUESTIONER", event);	
		
	}

}
