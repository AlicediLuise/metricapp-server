package metricapp.utility.stateTransitionUtils.graphTransition;

import metricapp.entity.Element;
import metricapp.entity.graph.Graph;
import metricapp.service.repository.BusApprovedElementRepository;

public class CreatedToApproved extends GraphStateTransitionCommand {
	
	public CreatedToApproved(Element before, Element after) {
		super(before, after);
	}

	@Override
	public void execute() throws Exception{
		super.execute();
		System.out.println("created to approved");
		
		String busVersion=BusApprovedElementRepository.getInstance().sendApprovedElement(after, Graph.class).getVersionBus();
		after.setVersionBus(busVersion);		
	}


}
