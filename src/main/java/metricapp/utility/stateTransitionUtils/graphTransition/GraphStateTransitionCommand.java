package metricapp.utility.stateTransitionUtils.graphTransition;

import metricapp.entity.Element;
import metricapp.entity.graph.Graph;
import metricapp.utility.stateTransitionUtils.StateTransitionCommand;

public class GraphStateTransitionCommand implements StateTransitionCommand {

	protected Graph before;
	protected Graph after;
	
	
	public GraphStateTransitionCommand(Element before, Element after) {
		this.before = (Graph) before;
		this.after = (Graph) after;
		
	}
	
	
	public void execute() throws Exception{
		if (before == null || after == null){
			throw new NullPointerException("Command needs to be initialized!");
		}
	}

}
