package metricapp.utility.stateTransitionUtils.graphTransition;

import metricapp.entity.Element;
import metricapp.entity.graph.Graph;
import metricapp.service.repository.BusApprovedElementRepository;

public class OnUpdateToApproved extends GraphStateTransitionCommand {

	public OnUpdateToApproved(Element before, Element after) {
		super(before, after);
	}
	
	@Override
	public void execute() throws Exception{
		super.execute();
		System.out.println("onUpdate to approved");
		
		String busVersion=BusApprovedElementRepository.getInstance().sendApprovedElement(after, Graph.class).getVersionBus();
		after.setVersionBus(busVersion);		
	}


}
