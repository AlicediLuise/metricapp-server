package metricapp.utility.stateTransitionUtils.graphTransition;

import metricapp.entity.Entity;
import metricapp.utility.stateTransitionUtils.AbstractStateTransitionFactory;

public class GraphStateTransitionFactory extends AbstractStateTransitionFactory {

	public GraphStateTransitionFactory(Entity entity) {
		super(entity);
	}
	
	// Initialization-on-demand holder idiom
    private static class SingletonHolder {
        private static final GraphStateTransitionFactory INSTANCE = new GraphStateTransitionFactory(Entity.Graph);
    }

    public static GraphStateTransitionFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }


}
