package metricapp.utility.stateTransitionUtils.graphTransition;

import metricapp.entity.Element;

public class CreatedToOnUpdate extends GraphStateTransitionCommand {
	
	public CreatedToOnUpdate(Element before, Element after) {
		super(before, after);
	}

	@Override
	public void execute() {
		System.out.println("From Created to OnUpdate");
	}

}
