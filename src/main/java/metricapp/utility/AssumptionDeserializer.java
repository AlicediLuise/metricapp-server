package metricapp.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import metricapp.entity.external.Assumption;
import metricapp.entity.external.ContextFactor;
import metricapp.entity.external.InstanceProject;
import metricapp.entity.external.OrganizationalGoal;

public class AssumptionDeserializer extends JsonDeserializer<Assumption> {
    @Override
    public Assumption deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	   ObjectCodec oc = jp.getCodec();
    	   JsonNode node = oc.readTree(jp);
    	   
    	   final String title = node.get("title").asText();
    	   final String description = node.get("description").asText();
    	   final String criticallyValue = node.get("criticalValue").asText();
    	   final String instanceProjectId = node.get("projectId").asText();
    	   final String state = node.get("status").asText();
    	   final String creationDate = node.get("creationDate").asText();
    	   //final String invalidationReason = node.get("invalidationReason").asText();

    	   
    	   Assumption c = new Assumption();
    	   c.setTitle(title);
    	   c.setDescription(description);
    	   c.setCriticallyValue(criticallyValue);
    	   c.setInstanceProjectId(instanceProjectId);
    	   c.setValidationState(state);
    	   c.setCreationDate(creationDate);
    	   return c;
    }
}