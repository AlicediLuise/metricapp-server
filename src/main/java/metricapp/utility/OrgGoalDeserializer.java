package metricapp.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import metricapp.entity.external.OrganizationalGoal;

public class OrgGoalDeserializer extends JsonDeserializer<OrganizationalGoal> {
    @Override
    public OrganizationalGoal deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	   ObjectCodec oc = jp.getCodec();
    	   JsonNode node = oc.readTree(jp);
    	   
    	   final Long id = node.get("id").asLong();
    	   final String title = node.get("title").asText();
    	   final String timeFrame = node.get("timeFrame").asText();
    	   final String object = node.get("object").asText();
    	   final String focus = node.get("focus").asText();
    	   final String magnitude = node.get("magnitude").asText();
    	   final String instanceProjectId = node.get("projectId").asText();
    	   
    	   ArrayList<String> orgScopeIds = new ArrayList<String>();
    	   final String organizationalScopeId = node.get("scope").get("id").asText();
    	   orgScopeIds.add(organizationalScopeId);
    	   
    	   ArrayList<String> strategyChildIds = new ArrayList<String>();
    	   JsonNode descStrNode = node.withArray("descendantStrategies");
    	   for(JsonNode el:descStrNode){
    		   strategyChildIds.add(el.get("id").asText());
    	   }
    	   
    	   ArrayList<String> subgoalIds = new ArrayList<String>();
    	   JsonNode subGoalsNode = node.withArray("subGoals");
    	   for(JsonNode e:subGoalsNode){
    		   subgoalIds.add(e.get("id").asText());
    	   }
 
 /*   	   phase 2.2 fields, but they not exist for phase 2.1: 
  * 		private String priority;
  * 		private String rationaleId;
  * 		private String organizationalGoalState;
  * 		private ArrayList<String> constraints;
  * 		private ArrayList<String> strategyFatherId;
  * 		private ArrayList<String> collaborativeGoalIds;
  *   		private ArrayList<String> conflictGoalIds;
 */
       	   	   
    	   OrganizationalGoal g = new OrganizationalGoal();
    	   g.setTitle(title);
    	   g.setTimeFrame(timeFrame);
    	   g.setObject(object);
    	   g.setFocus(focus);
    	   g.setMagnitude(magnitude);
    	   g.setInstanceProjectId(instanceProjectId);
    	   g.setOrganizationalScopeIds(orgScopeIds);
    	   g.setStrategyChildIds(strategyChildIds);
    	   g.setSubgoalIds(subgoalIds);
        	   
    	   return g;
    }
}