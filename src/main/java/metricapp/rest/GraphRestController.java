package metricapp.rest;

import metricapp.dto.graph.GraphCrudDTO;
import metricapp.dto.graph.GraphDTO;
import metricapp.exception.BadInputException;
import metricapp.exception.DBException;
import metricapp.exception.IllegalStateTransitionException;
import metricapp.exception.NotFoundException;
import metricapp.service.spec.controller.GraphCRUDInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/graph")
public class GraphRestController {
	
	@Autowired
	private GraphCRUDInterface controller;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<GraphCrudDTO> getGraphDTO(@RequestParam(value = "measurementGoalId", defaultValue = "NA") String measurementGoalId,
			@RequestParam(value = "bus", defaultValue = "false") String bus) {
		
		GraphCrudDTO dto = new GraphCrudDTO();
		
		try {
			if(!bus.equals("false")){
				dto = controller.getAllApproved();
				return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.OK);
			}
			else if (!measurementGoalId.equals("NA")) {
				dto = controller.getGraphByMeasurementGoalId(measurementGoalId);
				return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.OK);
			}
			else {
				dto = controller.getAll();
				return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.OK);
			}
		} catch (BadInputException e) {
			dto.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.BAD_REQUEST);
		} catch (NotFoundException e) {
			dto.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			dto.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<GraphCrudDTO> deleteGraphDTO(@RequestParam String id) {
		GraphCrudDTO dto = new GraphCrudDTO();
		try {
			controller.deleteGraphById(id);
		} catch (BadInputException e) {
			e.printStackTrace();
			dto.setError(e.getMessage());
			return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.BAD_REQUEST);
		} catch (Exception e){
			dto.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<GraphCrudDTO>(HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<GraphCrudDTO> putGraphDTO(@RequestBody GraphDTO dto) {
		GraphCrudDTO rensponseDTO = new GraphCrudDTO();
		try {
			return new ResponseEntity<GraphCrudDTO>(controller.updateGraph(dto), HttpStatus.OK);
		} catch (NotFoundException e) {
			rensponseDTO.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(rensponseDTO, HttpStatus.NOT_FOUND);
		} catch (DBException e) {
			rensponseDTO.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(rensponseDTO, HttpStatus.CONFLICT);
		} catch (BadInputException e) {
			rensponseDTO.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(rensponseDTO, HttpStatus.BAD_REQUEST);
		} catch (IllegalStateTransitionException e) {
			rensponseDTO.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(rensponseDTO, HttpStatus.FORBIDDEN);
		}
		catch (Exception e){
			rensponseDTO.setError(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<GraphCrudDTO>(rensponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
