package metricapp.entity.graph;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import metricapp.entity.Element;
import metricapp.entity.external.PointerBus;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public class Graph extends Element{
	
	private PointerBus measurementGoalId;
	private List<String> baselineHypotheses;
	private List<String> variationFactors;
	private String impactOnBaselineHypotheses;
	
	public Graph() {
		baselineHypotheses = new ArrayList<String>();
		variationFactors = new ArrayList<String>();
	}

}
