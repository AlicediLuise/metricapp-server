package metricapp.entity;

public enum Entity {
	Graph("phase2"),
	MeasurementGoal ("phase2"),
	Question("phase2"),
	Metric("phase2"),
	ContextFactor("phase0"),
	Assumption("phase0"),
	InstanceProject("phase0"),
	User("phase2"),
	OrganizationalGoal("phase1");
	
	private final String phase;
	
	private Entity(String phase){
		this.phase=phase;
	}
	
	public String getPhase(){
		return this.phase;
	}
}
