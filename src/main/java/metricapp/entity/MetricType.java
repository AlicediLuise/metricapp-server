package metricapp.entity;

public enum MetricType {
	
	base,
	derived,
	indicator;

}
