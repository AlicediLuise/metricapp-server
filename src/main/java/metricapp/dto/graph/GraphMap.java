package metricapp.dto.graph;

import metricapp.entity.graph.Graph;

import org.modelmapper.PropertyMap;

public class GraphMap extends PropertyMap<Graph, GraphDTO> {

	@Override
	protected void configure() {
		
		map().getMetadata().setId(source.getId());
		map().getMetadata().setCreatorId(source.getCreatorId());
		map().getMetadata().setReleaseNote(source.getReleaseNote());
		map().getMetadata().setVersion(source.getVersion());
		map().getMetadata().setTags(source.getTags());
		map().getMetadata().setState(source.getState());
		map().getMetadata().setCreationDate(source.getCreationDate());
		map().getMetadata().setLastVersionDate(source.getLastVersionDate());
		map().getMetadata().setVersionBus(source.getVersionBus());
		map().getMetadata().setEntityType(source.getEntityType());
		
		map().setBaselineHypotheses(source.getBaselineHypotheses());
		map().setImpactOnBaselineHypotheses(source.getImpactOnBaselineHypotheses());
		map().setMeasurementGoalId(source.getMeasurementGoalId());
		map().setVariationFactors(source.getVariationFactors());
	}

}
