package metricapp.dto.graph;

import metricapp.entity.graph.Graph;

import org.modelmapper.PropertyMap;

public class GraphDTOMap extends PropertyMap<GraphDTO, Graph> {

	@Override
	protected void configure() {
		map().setId(source.getMetadata().getId());
		map().setReleaseNote(source.getMetadata().getReleaseNote());
		map().setCreationDate(source.getMetadata().getCreationDate());
		map().setLastVersionDate(source.getMetadata().getLastVersionDate());
		map().setState(source.getMetadata().getState());
		map().setVersion(source.getMetadata().getVersion());
		map().setTags(source.getMetadata().getTags());
		map().setVersionBus(source.getMetadata().getVersionBus());
		map().setEntityType(source.getMetadata().getEntityType());
	}
}
