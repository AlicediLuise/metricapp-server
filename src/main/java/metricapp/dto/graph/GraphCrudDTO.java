package metricapp.dto.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import metricapp.dto.MessageDTO;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public class GraphCrudDTO extends MessageDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<GraphDTO> graphs; 
	private String scope;
	private String userIdCrud;
	private long count;
	

	public GraphCrudDTO(){
		this.setGraphs(new ArrayList<GraphDTO>());;
	}
	
	public void addGraphToList(GraphDTO graph){
		try{
			this.graphs.add(graph);
		}
		catch(NullPointerException e){
			this.graphs = new ArrayList<GraphDTO>();
			this.graphs.add(graph);
		}
		
	}

}
