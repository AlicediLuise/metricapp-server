package metricapp.dto.graph;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import metricapp.dto.DTO;
import metricapp.entity.external.PointerBus;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public class GraphDTO extends DTO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PointerBus measurementGoalId;
	private List<String> baselineHypotheses;
	private List<String> variationFactors;
	private String impactOnBaselineHypotheses;


}
